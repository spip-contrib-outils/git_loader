#!/bin/bash

version_gitloader="1.0.0"
version="master"
dir_root="."
mode="http"

hash curl 2>/dev/null || { echo >&2 "curl is required. Stopped."; exit 1; }
hash jq 2>/dev/null || { echo >&2 "jq is required. Stopped."; exit 1; }
hash git 2>/dev/null || { echo >&2 "git is required. Stopped."; exit 1; }

while getopts "v:d:m:" opt; do
  case ${opt} in
    v) #version SPIP, par défaut master
			version="${OPTARG}"
      ;;
    d) #répertoire cible
			dir_root="${OPTARG}"
      ;;
    m) #git or http
			mode="${OPTARG}"
      ;;
    \?)
			echo "Version: $version_gitloader"
			echo "Usage: $0 [-v] [-d] [-m]"
			echo "-v : version de SPIP master ou x.y"
			echo -e "\\t par défaut master"
			echo "-d : répertoire cible où télécharger SPIP et ses plugins"
			echo -e "\\t par défaut : répertoire courant"
			echo "-m : mode de téléchargement, ssh ou http"
			echo -e "\\t par défaut http"
			exit;
      ;;
  esac
done


if [ ! -d "$dir_root" ]; then
	mkdir "$dir_root" || exit 1
fi

if [ ! -d "$dir_root/plugins-dist" ]; then
	mkdir "$dir_root/plugins-dist" || exit 1
fi

function clone_git() {
	local repo_git="$1"
	local repo_name="$2"
	local version="$3"

	if [ ! -d "$repo_name/.git" ]; then
		git init "$repo_name"
		git -C "$repo_name" remote add origin "$repo_git" >/dev/null 2>&1
	fi
	if ! git -C "$repo_name" fetch --all -t >/dev/null 2>&1; then
		git -C "$repo_name" config --unset-all remote.origin.fetch "\\+refs\\/svn\\/map:refs\\/notes\\/commits"
		git -C "$repo_name" fetch --all -t >/dev/null 2>&1
	fi
	if ! git -C "$repo_name" reset --hard "origin/$version" >/dev/null 2>&1; then
		git -C "$repo_name" checkout --orphan orphan
		git -C "$repo_name" reset --hard
	else
		git -C "$repo_name" checkout "$version" >/dev/null 2>&1
		git -C "$repo_name" branch -u "origin/$version" >/dev/null 2>&1
	fi


	#git -C "$repo_name" checkout "$version"
}

# Import SPIP
if [[ "$mode" == "git" ]] || [[ "$mode" == "ssh" ]];then
	source_git="git@git.spip.net:spip/spip.git"
fi
if [[ "$mode" == "http" ]];then
	source_git="https://git.spip.net/spip/spip.git"
fi

echo "Import SPIP core dans $dir_root à la version $version"
clone_git "$source_git" "$dir_root" "$version"

curl -s -X GET "https://git.spip.net/api/v1/orgs/spip/repos" -H  "accept: application/json" -o repos.json
last_index_repo=$(jq -r 'length - 1' repos.json)

if [ "$version" == "master" ]; then
  version_repo="master"
else
  compteur=$(echo "$version" | tr -cd '.' | wc -c | xargs)
  if [ "$compteur" -gt 1 ]; then
    version_repo="spip/$version"
  else
    version_repo="spip-$version"
  fi
fi

for i in $(seq 0 "$last_index_repo")
do
	repo_name=$(jq -r ".[$i] .name" repos.json)
	case "${repo_name}" in
		spip|ecrire|prive|dev|themes)
			continue
			;;
		*)
			;;
	esac

	if [[ "$mode" == "git" || "$mode" == "ssh" ]];then
		source_git=$(jq -r ".[$i] .ssh_url" repos.json)
	fi
	if [[ "$mode" == "http" ]];then
		source_git=$(jq -r ".[$i] .html_url" repos.json)
	fi

	repo_path="$dir_root/plugins-dist/$repo_name"

	#Une exception dans les plugins, le dépot dist n'en ai pas un
	if [ "$repo_name" == "dist" ]; then
		repo_path="$dir_root/squelettes-$repo_name"
	fi

	echo "Import de $repo_name à la version $version_repo ("$source_git")"
	clone_git "$source_git" "$repo_path" "$version_repo"
done

rm repos.json