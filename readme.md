# Importer la distribution SPIP via git

## Introduction

> Ce script permet d'importer un environnement SPIP uniquement via git (core et plugins-dist)


## Usage : Création d'un projet SPIP 

Le script importe un environnement SPIP dans la destination (-d) à la version (-v) via un git clone http/ssh (-m)

```
git_loader/git_loader.sh : option non permise -- h
    Version: x.y.z
    Usage: git_loader/git_loader.sh [-v] [-d] [-m]
        -v : version de SPIP master ou x.y (branche) ou x.y.z (tag)
             par défaut master
        -d : répertoire cible où télécharger SPIP et ses plugins
             par défaut : répertoire courant
        -m : mode de téléchargement (ssh ou http)
             par défaut http
```

L'option _-m_ intéressera plus particulièrement les personnes ayant un accès SSH aux dépots.

### Pour avoir une version de developpement SPIP

```
cd /opt/scripts/spip/git_loader
bash git_loader.sh -d /var/www/spip-dev/ -m http -v master
```

Les arguments étant optionnels on peut écrire :
```
bash git_loader.sh -d /var/www/spip-dev/
```

### Pour avoir une version donnée

Le script ne permet pas de suivre une branche. Il suit uniquement des version tagguées.

```
cd /opt/scripts/spip/git_loader
bash git_loader.sh -d /var/www/spip-prod/ -v 3.1.12
```

Il est possible de simplifier ceci
```
bash git_loader.sh -d /var/www/spip-dev/
```

Note : si un plugin présent n'a pas de version spip/vX.Y.Z son répertoire sera initialisé à un état vide.

### Mise à jour / Changement de version

Ce script permet de basculer vers n'importe quelle version SPIP. Il est par exemple possible de passer d'une version 3.1 vers 3.2 
```
cd /opt/scripts/spip/git_loader
bash git_loader.sh -d /var/www/spip-prod/ -v 3.1.12
bash git_loader.sh -d /var/www/spip-prod/ -v 3.2.7
```


## Contribuer

Les correctifs sont les bienvenus.
